# Variable to Input a Decimal Number
decimal_num = int(input('Enter a Computing Decimal Number:'))

# An empty list, datasets/binary info will be added later automatically in the prompt
binary_storage_list = []

'''
Using while loop instead of if else conditions
because while coding there were no limits to the iterable
'''
while(decimal_num != 0):
    
    # It is like a modular machine. It takes decimals and modules it by 2.
    # Moduler in simple term is the first reminder while dividing
    moduler = decimal_num % 2
    
    # It adds the binary info to the list automatically (explain append)
    binary_storage_list.append(moduler)
    
    # Decimal Num Float Division like Decimal(After Point [.]) is removed in the division.
    decimal_num = decimal_num//2

# While Doing Binary You go from down to up/last to first, by default in python it is up to down  
binary_storage_list.reverse()


# This alltogether Print Statements are there in the prompt
# (Total {9 Line: Code}{15 Lines: Comments}{11 Lines: Enters}{Total No of Lines:35})


# Print Statement for Undersatnding
print('The Binary Number is:')

# It removes {spaces}{square brackets}{commas} from the List
print(''.join(map(str, binary_storage_list)))